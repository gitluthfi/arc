import os
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from oauth2client.service_account import ServiceAccountCredentials

def backup_and_upload_to_drive(source_path, destination_folder_id):
    # Authenticate with Google Drive using the service account key file
    gauth = GoogleAuth()
    gauth.credentials = ServiceAccountCredentials.from_json_keyfile_name(
        './ily-bot-405907-3118d738aca8.json', 
        ['https://www.googleapis.com/auth/drive']
    )

    # Create a GoogleDrive client
    drive = GoogleDrive(gauth)
    print(f"Authenticated as: {gauth.credentials.service_account_email}")

    # Get the name of the source (either file or folder)
    source_name = os.path.basename(source_path)

    # Create a folder or file in Google Drive
    if os.path.isdir(source_path):
        folder_drive = drive.CreateFile({'title': source_name, 'mimeType': 'application/vnd.google-apps.folder', 'parents': [{'id': destination_folder_id}]})
        folder_drive.Upload()

        # Upload all files and subdirectories in the folder
        for item_name in os.listdir(source_path):
            item_path = os.path.join(source_path, item_name)
            backup_and_upload_to_drive(item_path, folder_drive['id'])

    elif os.path.isfile(source_path):
        file_drive = drive.CreateFile({'title': source_name, 'parents': [{'id': destination_folder_id}]})
        file_drive.SetContentFile(source_path)
        file_drive.Upload()

    folder_link = file_drive['alternateLink']
    #folder_link = drive.CreateFile({'id': folder_drive['id']}).Get('alternateLink') if os.path.isdir(source_path) else None
    print(f"Backup completed successfully. {'Folder' if os.path.isdir(source_path) else 'File'} link: {folder_link}")

if __name__ == "__main__":
    source_path = "/var/www/ily_bot/backup.py"
    destination_folder_id = "1EUanL_z1gAG8UbWUXN0CPLpjUiiTSb8D"

    # Backup and upload to Google Drive
    backup_and_upload_to_drive(source_path, destination_folder_id)
