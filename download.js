const fileUrls = [
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/592827f2164e4ef6b7435981912eac93.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/56c45f6ffed843d292742ec1cf1c111a.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/792a1532f9d646a5a1cc119bf09e0931.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/7efbc70925094639a624a8a0b3a976fe.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/6c97126661344cd58fd686f09e512fca.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/c593f9e148e34554896dd969ff391b4e.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/eb9a537d480f40f79e895f3a1128d3b0.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/0bd1695d20c6465193895f98db959367.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/ac337887b5214c57968d80c885bcab55.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/4efbb929d12b4ddea1c93bbb2775fb50.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/d569fcc35e4843f5b98d9d8813a3fb8c.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/3682e214231b482fa80d96f784c80e4c.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/be774ab09e23473fac15636d6064e215.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/66739a35198242dcbce13e80c32c27f6.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/55df56357f2c4bbdbf6b7de6abb9377a.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/a4d54c338d554d52b1cd2a12c43dd3a1.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/a1d4d761e0864086bb386b83c289dccc.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/e65db2222d5749ea9406b6ee49bf137a.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/435d8817a9a041948940be6f8b6984ff.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/372051a7336f4a099e0ecbcf78f662c6.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/2aee4087f61d4912b7605cbcdaedfdb2.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/046e198ad5b845b78320b9bad4dd3300.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/ccdacf5bdf004d69ab126a95d3cfc883.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/801211a635e5423a838af2a611d9dbd4.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/333c7696a8c14ea58f54c6a34c2ca7a5.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/b6065cb13d7642dbbb5223c92f401f82.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/aa1d3d36ea46451aa9b5d3dd7e315fb0.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/8febf4235bfe43e59c0bf1afa92d62d5.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/8e1a0fd513a04249b04af30af4fa4ecd.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/1cc0836cd15c492592700a9f36ac02f5.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/28c020e23ed04573bc07b7c2fcbc92b0.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/53d2dc0523d14e7bb8f876083b918297.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/99ef76f2cc5248d5b205b9d2265be443.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/8154b83d71e64f1ca1d3ca9ce3e48396.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/8385511f8e0a4505818f884c193da907.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/84cc5d1b09684c9f870ddc302d25d9cd.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/e2aeb2ea5fcc46098979a9534fc9b84d.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/42959292bc854d659b260e81bc35a4f1.pdf',
    'https://sitren.kemenag.go.id/storage/public/uploads/path_sk_signed/6d92b4e819cf472082efee32ae49d903.pdf'
];

function downloadFile(url) {
    fetch(url)
        .then(response => response.blob())
        .then(blob => {
            const a = document.createElement('a');
            const url = window.URL.createObjectURL(blob);
            a.href = url;
            a.download = url.substring(url.lastIndexOf('/') + 1);
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        })
        .catch(error => console.error('Error downloading file:', error));
}

// Trigger file downloads
fileUrls.forEach(url => downloadFile(url));