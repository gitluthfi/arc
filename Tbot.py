# fix uploader id error
# watch this bro, this unreal
# https://www.youtube.com/watch?v=Ghe058HpmMk&ab_channel=JaredThomas

import docker
import os
import ssl
import subprocess
import re
import asyncio
import pandas as pd
import io
import discord
import aiohttp
import gitlab
import csv
from dotenv import load_dotenv
from discord.ext import commands
from urllib.request import build_opener



import certifi
import urllib3
import urllib

# Set the SSL certificate path
urllib3.util.ssl_.DEFAULT_CERTS_FILE = certifi.where()

load_dotenv()
# TOKEN = os.getenv('DISCORD_TOKEN')
# TOKEN_PROD = os.getenv('DISCORD_TOKEN_PROD')
DB_HOST = os.getenv('DB_HOST')
DB_PORT = os.getenv('DB_PORT')
DB_USER = os.getenv('DB_USERNAME')
DB_PW = os.getenv('DB_PASSWORD')
PATH_FILE= os.getenv('PATH_FILE')
ENV= os.getenv('BOT_ENV')
FOLDER_ID = os.getenv("FOLDER_DRIVE")
GENIUS_API = os.getenv('GENIUS_API_ID')
#docker_client = docker.from_env()
genius = lyricsgenius.Genius(GENIUS_API)
intents = discord.Intents.default()
intents.members = True
bot = commands.Bot(command_prefix='!', intents=intents)
sslcontext = ssl.create_default_context()
sslcontext.check_hostname = False
sslcontext.verify_mode = ssl.CERT_NONE
ssl._create_default_https_context = ssl._create_unverified_context
ssl_context = ssl._create_unverified_context()
opener = build_opener(urllib.request.HTTPSHandler(context=ssl_context))
connector = aiohttp.TCPConnector(ssl=sslcontext)
client = discord.Client(connector=connector, intents=intents)

queue = deque()


urllib3.util.ssl_.DEFAULT_CERTS_FILE = certifi.where()

# Disable SSL certificate verification
sslcontext = ssl.create_default_context()
sslcontext.check_hostname = False
sslcontext.verify_mode = ssl.CERT_NONE


@client.event
async def on_ready():
    # CREATES A COUNTER TO KEEP TRACK OF HOW MANY GUILDS / SERVERS THE BOT IS CONNECTED TO.
    guild_count = 0

    # LOOPS THROUGH ALL THE GUILD / SERVERS THAT THE BOT IS ASSOCIATED WITH.
    for guild in client.guilds:
        # PRINT THE SERVER'S ID AND NAME.
        print(f"- {guild.id} (name: {guild.name})")

        # INCREMENTS THE GUILD COUNTER.
        guild_count = guild_count + 1
        # Check if there is a channel named "general" in the guild
        general_channel = discord.utils.get(
            guild.text_channels, name="general")

        if general_channel:
            # Send a message in the "general" channel
            if general_channel.permissions_for(guild.me).send_messages:
                await general_channel.send("@everyone ILY ready to serve!")

@client.event
async def on_message(message):
    global voice_clients
    if (ENV == "production"):
        perintah = '!ARC'
    elif (ENV == "develop"):
        perintah = '!ILO'
    if message.author == client.user:
        return

    if message.content.startswith(f"{perintah} hello"):
        await message.reply("Hello")
    elif message.content.startswith(f"{perintah} berikan aku ruang"):
        # Mengambil server (guild) tempat pesan tersebut diposting
        guild = message.guild
        parts = message.content.split(" ")
        if len(parts) >= 5:
            namespace = " ".join(parts[4:])

    # Membuat saluran teks baru dengan nama "ruang-<nama_pengguna>"
        channel_name = f"ruang-{namespace}"
        new_channel = await guild.create_text_channel(channel_name)

    # Mengirim pesan konfirmasi ke saluran teks yang asal
        await new_channel.send(f"Ruang teks baru '{channel_name}' telah dibuat untuk {message.author.mention}")
    #                               #
    #INI MASIH DALAM TAHAP UJI COBA #
    #                               #
    elif message.content.startswith(f"{perintah} aku mau ngomong sesuatu ra"):
        guild = message.guild

    # Membuat kategori (category) untuk thread baru
        category = await guild.create_category(f"Ruang-{message.author.name}")

    # Membuat thread baru dalam kategori tersebut
        thread = await category.create_text_channel(name="ruang-teks", reason="Membuat thread baru")

    # Mengirim pesan ke dalam thread yang baru saja dibuat
        await thread.send(f'kemari ali! {message.author.mention}')
    # batas uji coba
    elif message.content.startswith('!HAI'):
        await message.reply(f'hai, {message.author.mention}')
    elif message.content.startswith('hai, ra!'):
        await message.reply('hai, ali')
    elif message.content.startswith(f"{perintah} show_databases"):
        cmd = f"mysql -h {DB_HOST} -u {DB_USER} -e 'select DB_ILY.m_staging.database_name, DB_ILY.m_staging.project_name from DB_ILY.m_staging;'"
        
        if message.content.startswith(f"{perintah} show_databases"):
            parts = message.content.split(" ")
            if len(parts) >= 2:
                link = " ".join(parts[2:])
                cmd = f"mysql -h {DB_HOST} -u {DB_USER} -e 'select DB_ILY.m_staging.database_name, DB_ILY.m_staging.project_name from DB_ILY.m_staging where project_name like \"%{link}%\";'"
                # print(cmd)
        try:
            result = subprocess.run(
                cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, text=True)
            if result.returncode == 0:
                output = result.stdout
            else:
                output = f"Error: {result.stderr}"
        except Exception as e:
            output = f"An error occurred: {e}"

        await message.reply(output)
    elif message.content.startswith(f"{perintah} deploy"):

        if len(message.content.split(" ")) == 4:
            
        # parse
            project_name = message.content.split(" ")[2]
            environment = message.content.split(" ")[3]        
            path_project = '/var/lib/jenkins/workspace/' + project_name
            
            with open('project_mapping.json') as f:
                project_mapping = json.load(f)

            if project_name not in project_mapping:
                await message.reply(f"Sir {message.author.mention} project tidak ditemukan, segera hubungi admin")
                return
            
            # deklarasi ulang
            project_details = project_mapping[project_name]
            build_path = project_details['build_path']
            ports = project_details['ports']
            additional_volumes = project_details['additional_volumes']

            # checking envireonment
            if (environment == "staging"):
                tag = "latest"
            else:
                await message.reply("environment tidak ditemukan")
            #debugging 
            print(f"Project Name: {project_name}")
            print(f"Tag: {tag}")
            print(f"Environment: {environment}")

            await message.reply(f"Mohon tunggu sir {message.author.mention}, pesanan anda sedang di proses")
        else:
            await message.reply(f"Sir {message.author.mention}, perintah tidak lengkap mohon lenkapi seperti ini \n `!ILY deploy project_name environment`")    
            return
        
        # Check if a container with the specified project name and tag exists
        container_name = f"{project_name}-{environment}"
        existing_container = None

        for container in docker_client.containers.list(all=True):
            if container.name == container_name:
                existing_container = container
                break

        # Stop and remove the existing container if it exists
        if existing_container:
            existing_container.stop()
            existing_container.remove()

        # Check and cleanup docker image    
        image_name = f"{project_name}:{tag}"
        existing_image = None
        print(image_name)

        for img in docker_client.images.list(name=project_name):
            existing_image = img
            break
        
        if existing_image:
            docker_client.images.remove(project_name)
        await message.reply(f"Sir {message.author.mention}, Pesananmu sedang dalam proses build, tunggu sebentar ya!! \n Sambil menunggu proses build, nikmati layanan musik saya, dapat diakses dengan command {perintah} play judul_lagu_anda")
        # Build docker file
        docker_client.images.build(
            path=build_path,
            tag=image_name,
            rm=True
        )
        
        await message.reply(f"sir {message.author.mention}, Pesanan sedang di rilis")
        # Run a new container with port mapping and volume attachment
        container = docker_client.containers.run(
            image=image_name,
            detach=True,
            ports=ports,
            volumes={**{build_path: {'bind': '/var/www/html/app', 'mode': 'rw'}}, **additional_volumes},
            name=container_name
        )

        await message.reply(
            f"{project_name} berhasil di deploy sir, {message.author.mention} \n Container ID: {container.id} \n Container Name: {container.name} \n Container Image: {container.image.tags[0]}"
        )

    elif message.content.startswith(f"{perintah} exec"):
        # parse
        parts = message.content.split(" ")
        base_path = '/var/lib/jenkins/workspace/'

        # Check if there are at least 3 parts (command, database name, and user query)
        if len(parts) >= 5 and parts[3] == "--command=":
            # Extract the database name and user command
            project_name = parts[2]
            user_command = " ".join(parts[4:])

            # Map project names to directory names 
            #dynamic project mapping
            project_mapping = await read_project_mapping_from_csv("project.csv")

            # Check if the project_name is valid
            if project_name in project_mapping:
                dir_project = base_path + project_mapping[project_name]

                # Construct the command
                cmd = f"cd {dir_project} && {user_command}"
                print(cmd)
                exit()
                try:
                    result = subprocess.run(cmd, shell=True, check=True, capture_output=True, text=True)
                    chunk_size = 1900  # batas discord ngirim pesan cuy
                    chunks = [result.stdout[i:i + chunk_size] for i in range(0, len(result.stdout), chunk_size)] #satuin setiap command yang kepecah karena chungks
                    for chunk in chunks: # kirim setiap chunks
                        await message.reply(f"Command executed successfully:\n```{chunk}```")
                except subprocess.CalledProcessError as e: #eror handling
                    await message.reply(f"Command failed with error:\n```{e.stderr}```")
            else: # error handling
                await message.reply(f"Invalid project name: {project_name}")
        else: # error handling
            await message.reply(f"Invalid command format, `{perintah} project_name exec --command= your_command_here`")

    elif message.content.startswith(f"{perintah} --help"):
        await message.reply(f"`{perintah} play judul_lagu` = Untuk putar lagu \n `{perintah} backup nama_database` = Untuk backup database STAGING \n `{perintah} run nama_database query_anda` = Untuk meng-query database \n `{perintah} get nama_database query_anda` = Untuk mendapatkan hasil dari query \n `{perintah} deploy nama_project envronment` = Untuk deploy by docker dengan build dan run as container \n `{perintah} exec nama_project --command= command_anda` = Untuk menjalankan command di server")

@bot.event
async def on_message(message):
    # Ignore messages from the bot itself
    if message.author == bot.user:
        return

    try:
        # Process commands if applicable
        await bot.process_commands(message)
    except Exception as e:
        # Handle exceptions gracefully
        print(f"An error occurred while processing a command: {e}")

    # Check if the author is valid before accessing attributes
    if message.author is not None:
        # Now you can safely access attributes of message.author
        author_id = message.author.id
        author_name = message.author.name
        print(
            f"Message received from user {author_name} (ID: {author_id}): {message.content}")
    else:
        print("Received a message with no valid author.")

    await bot.process_commands(message)

if (ENV == "production"):
    TOKEN = os.getenv('DISCORD_TOKEN_PROD')
else:
    TOKEN = os.getenv('DISCORD_TOKEN')

client.run(TOKEN)


